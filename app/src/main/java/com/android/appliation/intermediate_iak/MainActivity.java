package com.android.appliation.intermediate_iak;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.weather_day_title)
    TextView weatherDayTitle;

    @BindView(R.id.weather_desc)
    TextView weatherDesc;

    @BindView(R.id.weather_temperatur)
    TextView weatherTemperature;

    @BindView(R.id.img_weather)
    ImageView imgWeather;

    @BindView(R.id.weather_list)
    RecyclerView weatherList;

    private List<Weather> weatherListData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        //this one for header
        weatherDayTitle.setText("Hari Minggu");
        weatherDesc.setText("Cukup Terang");
        weatherTemperature.setText("20 Derajat");
        imgWeather.setImageResource(R.mipmap.ic_launcher_round);
        //end header

        //this one for Recycler View
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        weatherList.setLayoutManager(llm);

        //to Weather
        weatherListData = new ArrayList<Weather>();
        weatherListData.add(new Weather(R.mipmap.ic_launcher, "11 November 2017","Cerah","17"));
        weatherListData.add(new Weather(R.mipmap.ic_launcher, "12 November 2017","Mendung","18"));
        weatherListData.add(new Weather(R.mipmap.ic_launcher, "13 November 2017","Hujan","19"));
        weatherListData.add(new Weather(R.mipmap.ic_launcher, "14 November 2017","Cerah","20"));
        weatherListData.add(new Weather(R.mipmap.ic_launcher, "15 November 2017","Mendung","21"));
        weatherListData.add(new Weather(R.mipmap.ic_launcher, "16 November 2017","Hujan","22"));


        WeatherAdapter weatherAdapter = new WeatherAdapter(weatherListData);

        //setRecyclerView
        weatherList.setAdapter(weatherAdapter);
    }
}
